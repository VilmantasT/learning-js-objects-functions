var a = ["js", "php"],
    b = {"lang": "js", "difficulty": "advanced"}
    c = "Hello world!",
    d = 2019,
    e = null,
    f = function() { console.log('ha-ha!') },
    g = new Error();

function checkType(test) {

    if (Object.prototype.toString.call(test) === '[object Array]') {
        console.log('Array! -- ', test, Object.prototype.toString.call(test));
    } else if (Object.prototype.toString.call(test) === '[object Object]') {
        console.log('Object! -- ', test, Object.prototype.toString.call(test));
    } else {
        console.log('Can\'t tell - neither Array, nor Object! -- ', test, typeof(test));
    };
}

checkType(a);
checkType(b);
checkType(c);
checkType(d);
checkType(e);
checkType(f);
checkType(g);    // expect to have error dump in console after this