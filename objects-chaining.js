var myMath = {
    value: 1,
    set: function(n) {
        this.value = n;
        return this;
    },
    increment: function() {
        this.value = this.value + 1;
        return this;
    },
    decrement: function() {
        this.value = this.value - 1;
        return this;
    },
    add: function(n) {
        this.value = this.value + n;
        return this;
    },
    sub: function(n) {
        this.value = this.value - n;
        return this;
    },
    log: function() {
        console.log(this.value);
    }
}

myMath.increment().add(5).log();
myMath.decrement().log();
myMath.set(0).add(15).sub(3).log();
