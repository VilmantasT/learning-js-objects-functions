setupCounterFunc = function() {
    var count = 0;

    return function() {
        count = count + 1;
        console.log(count);
        return count;
    }
}

var increment = setupCounterFunc();

increment();
increment();
increment();