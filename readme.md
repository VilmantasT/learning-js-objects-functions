# Topics

- JavaScript data types
- JavaScript functions and constructions
- JavaScript objects, constructions and patterns

Have NodeJS installed.

Run scripts in terminal:

`node objects-basic.js`
