(function(global) {
    console.log('global is ', global);

    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        today = new Date(),
        msg = 'Today is ' + days[today.getDay()];

    console.log(msg);
    console.log(today);
})(this);

console.log('Just after immediate function expresion.. Something happened?');